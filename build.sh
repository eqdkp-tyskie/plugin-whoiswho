#!/usr/bin/env bash

if [[ $# -eq 0 ]] ; then
    echo "mission version"
    echo "usage: ${0} version"
    exit 1
fi
VERSION=${1}
NAME="whoiswho"
ZIP_FILE="${NAME}-${VERSION}.zip"

if [[ -z ${CI_COMMIT_SHORT_SHA+x} ]]; then
    BUILD_ID="$(find plugin-${NAME} -type f -exec md5sum {} \; | sort -k 2 | md5sum | head -c 5)"
else
    BUILD_ID=${CI_COMMIT_SHORT_SHA}
fi
TIMESTAMP=$(date +%s)

if [[ -f "${ZIP_FILE}" ]]; then
    echo "File exist, aborting"
    exit 1
fi
cp -R plugin-${NAME} plugin-${NAME}-${VERSION}
cp package.xml.tpml package.xml

sed -i "s/<version>PLACEHOLDER<\/version>/<version>${VERSION}<\/version>/" package.xml
sed -i "s/<creationDate>PLACEHOLDER<\/creationDate>/<creationDate>${TIMESTAMP}<\/creationDate>/" package.xml
sed -i "s/public \$version = 'VERSION_PLACEHOLDER'/public \$version = '${VERSION}'/" plugin-${NAME}-${VERSION}/${NAME}_plugin_class.php
sed -i "s/public \$build = 'BUILD_PLACEHOLDER'/public \$build = '${BUILD_ID}'/" plugin-${NAME}-${VERSION}/${NAME}_plugin_class.php

zip -r ${ZIP_FILE} plugin-${NAME}-${VERSION}/ package.xml

rm -r plugin-${NAME}-${VERSION} package.xml