from dotenv import load_dotenv

import json
import os
import requests
import sys

"""
Usage:

Set the variable in an .env file:

python test.py Name
"""

load_dotenv()

API_TOKEN=os.environ.get("API_TOKEN", "your token")
EQDKP_BASE_URL=os.environ.get("EQDKP_BASE_URL", "your eqdkp installation url")
EQDKP_MULTIDKP_ID=os.environ.get("EQDKP_MULTIDKP_ID", "1")

name = sys.argv[1]
data = [
    "function=whoiswho",
    "multidkp_id={EQDKP_MULTIDKP_ID}",
    "format=json",
    f"name={name}",
]
url = f"{EQDKP_BASE_URL}/api.php?{'&'.join(data)}"
headers = {
    'Authorization': API_TOKEN,
    "User-Agent": "test",
}

try:
    response = requests.get(url, headers=headers)
    print(json.dumps(response.json()))
except requests.exceptions.MissingSchema:
    print("ERROR: Missing scheme in the url. You must specify http:// or https:// in the eqdkp base url")
    sys.exit(1)
except:
    print(response.text)
