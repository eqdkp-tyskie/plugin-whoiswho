<?php

if (!defined('EQDKP_INC')){
    die('Do not access this file directly.');
}

class whoiswho extends plugin_generic{
    // Note: Dont touch these, they are used in automation
    public $version = 'VERSION_PLACEHOLDER';
    public $build = 'BUILD_PLACEHOLDER';
    // EndNote

    public $copyright = 'Tyskie';
    public $vstatus = 'Stable';

    protected static $apiLevel = 23;

    public function pre_install() {
        $this->config->set(
            $this->create_default_settings(),
            '0',
            $this->get_data('code')
        );
    }

    public function pre_uninstall() {
        $this->config->del(
            array_keys($this->create_default_settings()),
            $this->get_data('code')
        );
    }

    public function __construct(){
        parent::__construct();

        $this->add_data(array (
            'name'             => 'Whoiswho',
            'code'             => 'whoiswho',
            'path'             => 'whoiswho',
            'contact'          => 'tyskie.p99@gmail.com',
            'icon'             => 'fa-id-card-o',
            'version'          => $this->version,
            'author'           => $this->copyright,
            'description'      => "Provides an exchange to get the points and list of chars of a user's char",
            'long_description' => '(Exchange API) Return a list of all chars of a user for a given char name',
            'homepage'         => 'https://gitlab.com/eqdkp-tyskie/plugin-whoiswho',
            'template_path'    => 'plugins/whoiswho/templates/',
            'manuallink'       => false,
            'plus_version'     => '2.3',
            'build'            => $this->build,
        ));

        $this->add_dependency(array(
            'plus_version' => '2.3',
            'games'        => array(
                'eq',
            ),
        ));

        $this->add_permission(
            'a', 'config', 'N',
            $this->user->lang('configuration'), array(2,3)
        );

        $this->add_exchange_module('whoiswho');

        $this->add_menu('admin', $this->gen_admin_menu());
    }

    private function gen_admin_menu(){
        $admin_menu = array (array(
            'name' => $this->user->lang('whoiswho'),
            'icon' => 'fa-comment',
            1 => array (
                'link'  =>'plugins/' . $this->code . '/admin/settings.php'.$this->SID,
                'text'  => $this->user->lang('settings'),
                'icon'  => 'fa-wrench',
                'check' => 'a_' . $this->code . '_config',
            ),
        ));

        return $admin_menu;
    }

    private function create_default_settings() {
        $config_data = array(
            'protected' => '0',
        );
        return $config_data;
    }
}

?>
