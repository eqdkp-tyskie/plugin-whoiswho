# Build

```
# Change version number if you do any changes
# Build the zip file:
./build.sh
```

# Exchange settings

## Settings
`protected` Enable API authentication by token, disabled by default.

## Authentication
Authentication is required only if `protected` setting if turned on.

### Using API Keys

#### Core API Token

The Core API token can be found at the ACP >> Raids >> Data-Export. This Token has access to almost every exchange module (except the ones where a User is needed, e.g. raid signup). If you use this token, set the type of the token to "api".

#### Personal User Token

Every user has an own API Token. With these tokens, the whole Login process for Plus Exchange is not necessary anymore. The user token can be found at the personal user settings. If you use this token, set the type of the token to "user".

#### Passing the Token
Token are passed along headers
```
Authorization: "5e4e4b620d4b2afaec7b6192472f81b9bddb592a1914589c"
User-Agent:    "Something that is meaningful"
```

# Install

Head to your eqdkp plus administration panel -> Extension -> Extension Management -> Manual Upload

Select the zip file, and click upload

You can see the plugin in administration panel -> Extension -> Extension Management -> Plugins

# Test

Create a .env file with the following variables:

Change EQDKP_MULTIDKP_ID accordingly

```
API_TOKEN=""
EQDKP_BASE_URL=""
EQDKP_MULTIDKP_ID="1"
```

then run: `python test.py Name` where `Name` is the name of the char to lookup
