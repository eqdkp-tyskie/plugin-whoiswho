<?php

if (!defined('EQDKP_INC')){
    die('Do not access this file directly.');
}

if (!class_exists('exchange_whoiswho')){
    class exchange_whoiswho extends gen_class{
        public static $shortcuts = array('pex' => 'plus_exchange');
        public $options = array();

        public function get_whoiswho($params, $arrBody){
            $isAPITokenRequest = $this->pex->getIsApiTokenRequest();
            $shouldBeProtected = $this->config->get('protected', 'whoiswho');
            $isUserSigned = $this->user->is_signedin();

            if ($shouldBeProtected && !$isUserSigned && !$isAPITokenRequest){
                return $this->pex->error('access denied');
            }

            if(!isset($params['get']['name'])){
                return $this->pex->error('name required');
            }

            // This is tricky, this is the id of the multidkp rule
            // Usually set to 1 as first one, but it might be different in some installation
            // Thus being configurable
            $multidkp_id = 1;
            if(isset($params['get']['multidkpid'])){
                $multidkp_id = $params['get']['multidkpid'];
            }

            // Get the char id from submitted char name and deduct the user id and name.
            $char_id  = $this->pdh->get('member', 'id', array($params['get']['name']));
            if ($char_id == ""){
                return $this->pex->error("Character not found");
            }
            $main_id  = $this->pdh->get('member', 'mainid', array($char_id));
            if ($main_id == ""){
                return $this->pex->error("Main not found");
            }
            $user_id  = $this->pdh->get('member', 'user', array($char_id));
            if ($user_id == ""){
                return $this->pex->error("User not found");
            }
            $username = $this->pdh->get('user', 'name', array($user_id));
            if ($username == "" || $username == "Unknown"){
                return $this->pex->error("Username not found");
            }

            $remapped_chars = array();
            $other_member_ids = $this->pdh->get('member', 'other_members', array($char_id, true));
            $other_member_ids[] = $char_id;

            foreach($other_member_ids as $id){
                $is_main = false;
                if($id == $main_id){
                    $is_main = true;
                }
                // Prepare the points
                $char_points = $this->pdh->get('member', 'points', array($id, $multidkp_id));
                $points = array(
                    'current'     => $char_points[0],
                    'spent'       => $char_points[1],
                    'adjustment'  => $char_points[2],
                );
                // get profile data of that char
                $profile_data = $this->pdh->get('member', 'profiledata', array($id));
                $data = array(
                    'id'      => intval($id),
                    'name'    => $this->pdh->get('member', 'name', array($id)),
                    'points'  => $points,
                    'is_main' => $is_main,
                    'level'   => intval($this->pdh->get('member', 'level', array($id))),
                    'class'   => $this->pdh->get('member', 'classname', array($id)),
                    'race'    => $this->game->get_name('races', $profile_data['race']),
                    'guild'   => $profile_data['guild'],
                );
                // Sort the data by name
                ksort($data);
                $remapped_chars[$data['name']] = $data;
            }
            // Sort the char by name
            ksort($remapped_chars);

            // Get the current points
            $points_current = $this->pdh->get('points', 'current', array($main_id, $multidkp_id));

            return array(
                "user"       => $username,
                "points"     => $points_current,
                "characters" => $remapped_chars,
            );
        }
    }
}

?>
