<?php

if (!defined('EQDKP_INC')) {
    die('You cannot access this file directly.');
}

$lang = array(
    "whoiswho"       => 'Whoiswho',
    "Documentation"  => "Documentation",
    "Description"    => "Description",
    "Request"        => "Request",
    "Response"       => "Response",
    "Authentication" => "Authentication",

    "description_text"    => "This plugin expose an exchange endpoint in the api that returns"
        . " the characters and points linked to a user by passing a character name.",
    "authentication_text" => "If you enable the protection on the endpoint you will need to specify"
        . " your api token in the call using the follow header",
    "setting_protected_description" => "If enabled, the endpoint will only be available by passing an authorized"
        . " api key in the request.",

        "setting_protected_on_save_message_text"  => "The exchange endpoint is now protected by an api key.",
    "setting_protected_off_save_message_text" => "The exchange endpoint is no longer protected by api key.",
    "setting_protected_save_message_title"    => "Successfully saved",
    "warning_endpoint_not_protected"          => "Warning! The endpoint is public - anyone can access it.",

    "response_test_success"  => "For a given character name",
    "response_test_failure"  => "When a char or user is not found",
    "setting_protected_text" => "Protect with an api key",
);

?>
