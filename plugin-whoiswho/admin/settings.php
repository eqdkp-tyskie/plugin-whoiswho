<?php

define('EQDKP_INC', true);
define('IN_ADMIN', true);
define('PLUGIN', 'whoiswho');

$eqdkp_root_path = './../../../';

include_once($eqdkp_root_path.'common.php');

class WhoiswhoSettings extends page_generic {
    public static function __shortcuts() {
    $shortcuts = array('pm', 'user', 'config', 'core', 'in', 'jquery', 'html', 'tpl');
        return array_merge(parent::$shortcuts, $shortcuts);
    }

    public function __construct() {
        $this->user->check_auth('a_whoiswho_config');
        $handler = array(
            'save' => array('process' => 'save', 'csrf' => true),
        );
        parent::__construct(false, $handler);
        $this->process();
    }

    public function save() {
        $message = array(
            "title" => $this->user->lang("setting_protected_save_message_title"),
            "text"  => null,
            "kind"  => "success",
        );

        if($this->in->get("is_protected") == "on"){
            $message["text"] = $this->user->lang("setting_protected_on_save_message_text");
            $this->config->set("protected", "1", "whoiswho");
        }else{
            $message["kind"] = "warning";
            $message["text"] = $this->user->lang("setting_protected_off_save_message_text");
            $this->config->set("protected", "0", "whoiswho");
        };

        $this->display(array($message));
    }

    public function display($messages=array()){
        if($messages) {
            foreach($messages as $message) {
                $this->core->message($message["text"], $message["title"], $message["kind"]);
            }
        }

        $is_protected_setting_on = $this->config->get('protected', 'whoiswho');
        $this->tpl->assign_vars(array(
            "L_CONFIG"           => $this->user->lang("whoiswho")." ".$this->user->lang("settings"),
            "PROTECTED_CHECKBOX" => $is_protected_setting_on == "1" ? 'checked="checked"' : "",
            "IS_PROTECTED"       => $is_protected_setting_on == "1" ? true : false,
            "GUILDTAG"           => $this->config->get("guildtag"),
        ));

        $this->core->set_vars(array(
            'page_title'    => sprintf(
                $this->user->lang('admin_title_prefix'),
                $this->config->get('guildtag'),
                $this->config->get('dkp_name')).': '.$this->user->lang('configuration'
            ),
            'template_path' => $this->pm->get_data('whoiswho', 'template_path'),
            'template_file' => 'settings.html',
            'page_path'     => [
                [
                    'title'=>$this->user->lang('menu_admin_panel'),
                    'url'=>$this->root_path.'admin/'.$this->SID,
                ],
                ['title'=>$this->user->lang('whoiswho'), 'url'=>' '],
            ],
            'display'       => true,
        ));
    }
}

registry::register('WhoiswhoSettings');

?>
