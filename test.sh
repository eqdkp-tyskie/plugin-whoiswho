#!/usr/bin/env bash

# Just a simple php linter
echo "start php syntax check";
result=$(find plugin-whoiswho/ -type f -name \*.php -exec php -l {} \; | grep -ci "Errors parsing ")
exit ${result};